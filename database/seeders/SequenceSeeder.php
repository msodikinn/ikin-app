<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SequenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('m_sequence')->insert([
            'code' => 'B',
            'name' => 'Barang',
            'nomor' => 1,
        ]);

        DB::table('m_sequence')->insert([
            'code' => 'P',
            'name' => 'Pasien',
            'nomor' => 1,
        ]);
    }
}
