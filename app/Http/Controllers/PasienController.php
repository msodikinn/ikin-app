<?php

namespace App\Http\Controllers;

use App\Models\Pasien;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class PasienController extends Controller
{
    public function index()
    {
        $pasien = Pasien::latest()->paginate(5);
        
        return view('pasien.index',compact('pasien'))
                    ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('pasien.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:15'
        ]);
        //dapat no urut
        $brg = DB::table('m_sequence')->where('name', 'pasien')->first();
        // error_log('kode barang here '.$brg->code);
        // error_log('nomor barang here '.$brg->nomor);

        //dapat waktu
        $now = Carbon::now();
        // error_log('bulan here '.$now->format('y'));
        // error_log('tahun here '.$now->format('m'));
        //error_log('format here '.str_pad($brg->nomor, 4, '0', STR_PAD_LEFT));
        
        $codeBarang = $brg->code.'-'.$now->format('y').$now->format('m').str_pad($brg->nomor, 4, '0', STR_PAD_LEFT);

        $resp = Pasien::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'code' => $codeBarang
        ]);

        if ($resp->exists) {
            // success
            DB::table('m_sequence')
              ->where('name', 'pasien')
              ->update(['nomor' => $brg->nomor + 1]);
         }

        return redirect()->route('pasien.index')->with('status', 'pasien berhasil di tambah');
    }

    /**
     * Display the specified resource.
     */
    public function show(Pasien $pasien)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pasien $pasien)
    {
        return view('pasien.edit', compact('pasien'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pasien $pasien)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'phone' => 'required'
        ]);
        
        $pasien->name = $request->name;
        $pasien->phone = $request->phone;
        $pasien->save();
        return redirect()->route('pasien.index')->with('status', 'pasien berhasil di ubah');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pasien $pasien)
    {
        $pasien->delete();
        return redirect()->route('pasien.index')->with('status', 'pasien berhasil di hapus');
    }
}
