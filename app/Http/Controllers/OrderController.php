<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use App\Models\Order;

class OrderController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function index(Request $request)
    {

        $orders = DB::table('t_order')
            ->leftJoin('m_pasien', 't_order.pasien_id', '=', 'm_pasien.id')
            ->select('t_order.*', 'm_pasien.phone', 'm_pasien.name')
            ->get();
        
        return view('order.index',compact('orders'))
                    ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('order.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'pasien_id' => 'required'
        ]);
        //dapat no urut
        $resp = Order::create([
            'pasien_id' => $request->pasien_id,
        ]);

        return redirect()->route('order.index')->with('status', 'Transaksi berhasil di tambah');
    }

    public function edit(Order $order)
    {
        $hasil = DB::table('t_order')
            ->leftJoin('m_pasien', 't_order.pasien_id', '=', 'm_pasien.id')
            ->where('t_order.id', '=', $order->id)
            ->select('t_order.*', 'm_pasien.phone', 'm_pasien.name')
            ->first();


        $hasil2 = DB::table('t_order_detail')
            ->leftJoin('m_barang', 'm_barang.id', '=', 't_order_detail.barang_id')
            ->where('t_order_detail.order_id', '=', $order->id)
            ->select('t_order_detail.*', 'm_barang.name', 'm_barang.harga', DB::raw('m_barang.harga * t_order_detail.qty as total_harga'))
            ->get();

        $total = DB::select('select sum(tbl1.harga_total) total from (
            select mb.name, mb.harga, od.*, mb.harga * qty as harga_total from t_order_detail od 
            left join m_barang mb on (mb.id = od.barang_id)
            where order_id = ?
            ) tbl1', [$order->id]);

    
        return view('order.edit', compact('hasil', 'hasil2','total'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Barang $barang)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'harga' => 'required'
        ]);
        
        $barang->name = $request->name;
        $barang->harga = $request->harga;
        $barang->save();
        return redirect()->route('barang.index')->with('status', 'Barang berhasil di ubah');

    }
}
