<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Barang::latest()->paginate(5);
        
        return view('barang.index',compact('products'))
                    ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'harga' => 'required'
        ]);
        //dapat no urut
        $brg = DB::table('m_sequence')->where('name', 'barang')->first();
        // error_log('kode barang here '.$brg->code);
        // error_log('nomor barang here '.$brg->nomor);

        //dapat waktu
        $now = Carbon::now();
        // error_log('bulan here '.$now->format('y'));
        // error_log('tahun here '.$now->format('m'));
        //error_log('format here '.str_pad($brg->nomor, 4, '0', STR_PAD_LEFT));
        
        $codeBarang = $brg->code.'-'.$now->format('y').$now->format('m').str_pad($brg->nomor, 4, '0', STR_PAD_LEFT);

        $resp = Barang::create([
            'name' => $request->name,
            'harga' => $request->harga,
            'code' => $codeBarang
        ]);

        if ($resp->exists) {
            // success
            DB::table('m_sequence')
              ->where('name', 'barang')
              ->update(['nomor' => $brg->nomor + 1]);
         }

        return redirect()->route('barang.index')->with('status', 'Barang berhasil di tambah');
    }

    /**
     * Display the specified resource.
     */
    public function show(Barang $barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Barang $barang)
    {
        return view('barang.edit', compact('barang'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Barang $barang)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'harga' => 'required'
        ]);
        
        $barang->name = $request->name;
        $barang->harga = $request->harga;
        $barang->save();
        return redirect()->route('barang.index')->with('status', 'Barang berhasil di ubah');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Barang $barang)
    {
        $barang->delete();
        return redirect()->route('barang.index')->with('status', 'Barang berhasil di hapus');
    }
}
