<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Barang') }}
        </h2>
    </x-slot>

    <div class="py-12">
    <div class="mx-auto max-w-5xl sm:px-6 lg:px-8">
            <div class="overflow-hidden bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('pasien.update',$pasien->id) }}">
                        @csrf
                        @method('put')
                        <div class="mb-6">
                            <label class="block">
                                <span class="text-gray-700">Kode</span>
                                <input type="text" name="code"
                                    class="block w-full mt-1 rounded-md"
                                    placeholder="" value="{{old('code',$pasien->code)}}" />
                            </label>
                            @error('code')
                            <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-6">
                            <label class="block">
                                <span class="text-gray-700">Nama</span>
                                <input type="text" name="name"
                                    class="block w-full mt-1 rounded-md"
                                    placeholder="" value="{{old('name',$pasien->name)}}" />
                            </label>
                            @error('name')
                            <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-6">
                            <label class="block">
                                <span class="text-gray-700">Phone</span>
                                <textarea id="editor" class="block w-full mt-1 rounded-md" name="phone"
                                    rows="3">{{ $pasien->phone}}</textarea>
                            </label>
                            @error('content')
                            <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>

                        <x-primary-button type="submit">
                            Update
                        </x-primary-button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
